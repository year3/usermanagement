/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author L4ZY
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    static {
        userList.add(new User("admin", "1234"));
        userList.add(new User("user1", "1234"));
    }

    public static boolean addUser(User user) {
        userList.add(user);
        return true;

    }

    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }
    //update

    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }
    //read 1 user

    public static User getUser(int index) {
        if (index > userList.size() - 1) {
            return null;
        }
        return userList.get(index);

    }

    public static ArrayList<User> getUsers() {
        return userList;

    }

    //read all user
    public static ArrayList<User> searchUserName(String searchText) {
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(searchText)) {
                list.add(user);
            }
        }
        return userList;

    }

    //delete User
    public static boolean delUser(int index) {
        userList.remove(index);

        return true;
    }

    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    //login
    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public static void save() {

    }

    public static void load() {
 
    }

}
